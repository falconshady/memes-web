
'use client';

import { Button, Checkbox, Label, Modal, TextInput } from 'flowbite-react';

function ModalComponent({openModal, onCloseModal}) {
    

    return (
        <>
            <Modal show={openModal} size="md" onClose={onCloseModal} popup>
                <Modal.Header />
                <Modal.Body>
                    <div className="space-y-6">
                        <h3 className="text-xl font-medium text-gray-900 dark:text-white">Register into our
                            platform</h3>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="name" value="Your name"/>
                            </div>
                            <TextInput
                                id="name"
                                required
                            />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="email" value="Your email"/>
                            </div>
                            <TextInput
                                id="email"
                                required
                            />
                        </div>
                        <div>
                            <div className="mb-2 block">
                                <Label htmlFor="password" value="Your password"/>
                            </div>
                            <TextInput id="password" type="password" required/>
                        </div>
                        <div className="w-full">
                            <Button>Register</Button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    );
}

export default ModalComponent