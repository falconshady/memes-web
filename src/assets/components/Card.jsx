'use client';

import { Card } from 'flowbite-react';
import {useEffect, useState} from "react";

function CardComponent() {

    const [memes, setMemes] = useState([]);
    
    useEffect(() => {
        fetch('http://127.0.0.1:8000/api/memes/list/?page=1&rowsPerPage=10')
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                setMemes(data.response.data);
            });
    }, []);

    return (
        <>
            {memes.map(({id, name, description, url}) => (
                <Card
                    key={id}
                    className="max-w-sm"
                    imgAlt="Meaningful alt text for an image that is not purely decorative"
                    imgSrc={url}
                >
                    <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                        {name}
                    </h5>
                    <p className="font-normal text-gray-700 dark:text-gray-400">
                        {description}
                    </p>
                </Card>
            ))}
        </>
    );
}

export default CardComponent