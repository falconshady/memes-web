import { Tabs } from 'flowbite-react';
import { HiClipboardList} from 'react-icons/hi';

function TabsComponent({children}) {
    return (
        <div className="overflow-x-auto">
            <Tabs aria-label="Full width tabs" style="fullWidth">
                <Tabs.Item active title="Meme's List" icon={HiClipboardList}>
                    {children}
                </Tabs.Item>
            </Tabs>
        </div>
    );
}

export default TabsComponent