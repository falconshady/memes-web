'use client';

import './App.css'
import { Button } from 'flowbite-react';
import TabsComponent from "./assets/components/Tabs.jsx";
import CardComponent from "./assets/components/Card.jsx";
import ModalLoginComponent from "./assets/components/ModalLogin.jsx";
import ModalRegisterComponent from "./assets/components/ModalRegister.jsx";
import {useState} from "react";

function App() {
    const [openModalLogin, setOpenModalLogin] = useState(false);
    const [openModalRegister, setOpenModalRegister] = useState(false);

    function onCloseModalLogin() {
        setOpenModalLogin(false);
    }
    function onCloseModalRegister() {
        setOpenModalRegister(false);
    }
    
    return (
        <>
            <div className={"p-5"}>
                <Button color="blue" size="sm" className={"inline"} onClick={() => setOpenModalLogin(true)}>Login</Button> or <Button color="dark" size="sm" className={"inline"} onClick={() => setOpenModalRegister(true)}>Register</Button>
            </div>
            <TabsComponent>
                <div className={"grid grid-cols-5 gap-4"}>
                    <CardComponent></CardComponent>
                </div>
            </TabsComponent>
            <ModalLoginComponent openModal={openModalLogin} onCloseModal={onCloseModalLogin}></ModalLoginComponent>
            <ModalRegisterComponent openModal={openModalRegister} onCloseModal={onCloseModalRegister}></ModalRegisterComponent>
        </>
    )
}

export default App
